/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.5-10.1.24-MariaDB : Database - db_rhymes
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_rhymes` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `db_rhymes`;

/*Table structure for table `tbl_accounts` */

DROP TABLE IF EXISTS `tbl_accounts`;

CREATE TABLE `tbl_accounts` (
  `act_id` int(255) NOT NULL AUTO_INCREMENT,
  `act_email` varchar(255) DEFAULT NULL,
  `act_password` varchar(255) DEFAULT NULL,
  `fname` varchar(255) DEFAULT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `bday` date DEFAULT NULL,
  `bio` longtext,
  PRIMARY KEY (`act_id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_accounts` */

insert  into `tbl_accounts`(`act_id`,`act_email`,`act_password`,`fname`,`lname`,`bday`,`bio`) values (36,'ambayec.miguel@gmail.com','14cace4dfd0079896ab89776c95a6ceb','migs','ambayec','1902-01-01',NULL),(37,'frizzle@gmail.com','db68016846b6c655bd41c2a8e35b5f76','Frizzle Joy','Rejuso','1997-08-03',NULL);

/*Table structure for table `tbl_poems` */

DROP TABLE IF EXISTS `tbl_poems`;

CREATE TABLE `tbl_poems` (
  `poem_id` int(255) NOT NULL AUTO_INCREMENT,
  `poem` longtext,
  `title` varchar(255) DEFAULT NULL,
  `private` int(1) DEFAULT NULL,
  `act_id` int(255) DEFAULT NULL,
  `poem_date` datetime DEFAULT NULL,
  PRIMARY KEY (`poem_id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_poems` */

insert  into `tbl_poems`(`poem_id`,`poem`,`title`,`private`,`act_id`,`poem_date`) values (52,'<div><div style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: center;\">Ano nga ba ang meron ka na wala sa iba?&nbsp;</div><div style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: center;\">Ano bang meron ka na hinahanap-hanap ko pa?&nbsp;</div><div style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: center;\">Ano ba\'t nahuhulog ako sayo kahit di kita nakikita?&nbsp;</div><div style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: center;\">Bakit sa t\'wing nag-iisa ko\'y naiisip kita?&nbsp;</div><div style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: center;\">Sabi ko gagawin ko lahat ng paraan para malaman mo,&nbsp;</div><div style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: center;\">Ilalahad ako sayo ang lahat ng laman ng puso ko,&nbsp;</div><div style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: center;\">Ipapaalam ko ito kahit pa sa mundo,&nbsp;</div><div style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: center;\">At lahat ng sasabihin ko\'y isusulat ko sa bato.</div><div style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: center;\">&nbsp;Makinig ka sa tibok ng puso ko kahit saglit sana,&nbsp;</div><div style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: center;\">Pakinggan mo ang bawat talinhaga,&nbsp;</div><div style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: center;\">Na aking binibigkas sa bawat pag pintig,&nbsp;</div><div style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: center;\">Na syang hindi masabi ng mismo kong bibig.&nbsp;</div><div style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: center;\">Alamin ang lahat ng bagay na nasaking isip,&nbsp;</div><div style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: center;\">Unawain mo ang kilos ko kahit isang saglit,&nbsp;</div><div style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: center;\">Pangarap ko rin na mag kita tayo kahit sa panaginip,</div><div style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: center;\">&nbsp;At dun masasabi ko sayo ng di ko na kelangan pang ipilit.&nbsp;</div><div style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: center;\">Pero ano ba ang pinagkaiba ng panaginip sa reyalidad,&nbsp;</div><div style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: center;\">Ano bang kaya kong gawin sa oras na mulat ang mga mata,&nbsp;</div><div style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: center;\">Magagawa ko ba na masabi sayo\'t malaki ba ang posibilidad,&nbsp;</div><div style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: center;\">Na ang panaginip na naipikit ko ay maimulat ko na.&nbsp;</div><div style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: center;\">Gusto ko naman talaga maamin sayo na gusto kita,&nbsp;</div><div style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: center;\">Gusto ko naman talaga masabi habang mulat ang aking mata,&nbsp;</div><div style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: center;\">Ayoko na itago ito sa likod ng puso ko,&nbsp;</div><div style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: center;\">Ayoko na nasa pag tulog ko lang nasasabi sayo.</div><div style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: center;\">&nbsp;Na oo may nararamdaman ako para sayo,&nbsp;</div><div style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: center;\">Na oo araw araw kang nasa isipan Na oo ikaw nga yung tinitibok puso ko,&nbsp;</div><div style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: center;\">Na oo ikaw nga yung reyalidad na aking kahilingan&nbsp;</div><div style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: center;\">Ngunit kung ako man ay pagkakalooban ni Bathala,&nbsp;</div><div style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: center;\">Hihiling akong muli sa kanya,&nbsp;</div><div style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: center;\">At sasabihin ko sa kanya ang kahilingan ko,&nbsp;</div><div style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: center;\">Sasabihin ko sa kanya habang pikit ang mga mata ko.</div><div style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: center;\">&nbsp;Tatayo ako mismo sa harap ng gusto ko,&nbsp;</div><div style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: center;\">At sabay sasabihin kay Bathala ang katagang ito,&nbsp;</div><div style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: center;\">Panginoon sa pag dilat sana ng mga mata kong pinagkaloob mo,&nbsp;</div><div style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: center;\">Nawa\'y matupad ang lahat ng kahilingan ko.&nbsp;</div><div style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: center;\">Sana ang panaginip na ito pag dilat na aking mga mata,&nbsp;</div><div style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: center;\">Ay maging katotohanan na sa harap mismo nya,&nbsp;</div><div style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: center;\">At masabi ko na sana ang lahat ng nadarama,&nbsp;</div><div style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: center;\">Ng ang pag ibig ko sa kanya ay di na masayang pa.&nbsp;</div><div style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: center;\">-----------------by J.R Hael</div></div>','Sayang',0,1,'2017-10-22 00:10:14'),(53,'<div>i like hotto doggu</div><div>becoz i like coca cola</div>','Am a boi',0,33,'2017-10-22 00:21:35'),(58,'<div><b>Talalallalala</b></div>','abuji',1,37,'2017-10-27 03:01:00');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
