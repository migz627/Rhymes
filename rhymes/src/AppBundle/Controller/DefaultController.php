<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;      
use AppBundle\Entity\TblPoems;
use AppBundle\Entity\TblAccounts;
use AppBundle\Form\ComposePoem;
use AppBundle\Form\Myprofile;
use AppBundle\Form\Login;
use AppBundle\Form\Signup;
use Symfony\Component\HttpFoundation\Session\Session;

class DefaultController extends Controller
{
    private $session;

    public function __construct() {
        $this->session = new Session();
    }

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {

        if ($this->session->has("act_id")) {
            return new RedirectResponse('/home');
        }

        $signupSession = $this->session->get("signup");
        $this->session->remove("signup");

        $loginSession = $this->session->get("login");
        $this->session->remove("login");
        

        $accts = new TblAccounts();
        $loginForm = $this->createForm(Login::class, $accts);

        $signup = new TblAccounts();
        $signupForm = $this->createForm(Signup::class, $signup);


        $loginForm->handleRequest($request);
        if ($loginForm->isSubmitted() && $loginForm->isValid()){
            
            $repo = $this->getDoctrine()->getRepository(TblAccounts::class);
            $dbData = $repo->findOneBy(array("actEmail"=>$accts->getActEmail(), "actPassword"=>md5($accts->getActPassword())));
            
            if (!$dbData){
                $this->session->set("login", 1);
                return new RedirectResponse('/');
            } else {
                 $this->session->set("act_id", $dbData->getActId());
                $this->session->set("name", $dbData->getFname());
                $this->session->set("login", 2);
                return $this->redirectToRoute('home');
            }

        }
        
        $signupForm->handleRequest($request);
        if ($signupForm->isSubmitted() && $signupForm->isValid()){
            

            $data = $signupForm->getData();

            $em = $this->getDoctrine()->getManager();

            $accts->setFname($data->getFname());
            $accts->setLname($data->getLname());
            $accts->setBday($data->getBday());
            $accts->setActEmail($data->getActEmail());
            $accts->setActPassword(md5($data->getActPassword()));

            $em->persist($accts);

            $em->flush();

            $this->session->set("signup", 2);

            return $this->redirectToRoute('homepage');

        }

        // replace this example code with whatever you need 
        return $this->render('default/index.html.twig',
         ['loginForm'=>$loginForm->createView(),
          'signupForm'=>$signupForm->createView(),
          'login'=>$loginSession,
          'signup'=>$signupSession]);
    }

    /**
     * @Route("/myprofile", name="myprofile")
     */
    public function myprofileAction(Request $req)
    {
        if (!$this->session->has("act_id")) {
            return new RedirectResponse('/');
        }

        $act_id = $this->session->get("act_id");

        $poems = new TblPoems();
        $composePoem = $this->createForm(ComposePoem::class, $poems);

        $accts = new TblAccounts();
        $myprofile = $this->createForm(Myprofile::class, $accts);

        $myprofile->handleRequest($req);
        if ($myprofile->isSubmitted() && $myprofile->isValid()){

            $data = $myprofile->getData();

            $em = $this->getDoctrine()->getManager();
            $accts = $em->getRepository(TblAccounts::class) -> find($act_id);

            $accts->setLname($data->getLname());
            $accts->setFname($data->getFname()); 
            $accts->setBday($data->getBday());
            $accts->setBio($data->getBio());

            $em->persist($accts);

            $em->flush();

            $this->session->set("name",$data->getFname());
            return $this->redirectToRoute('myprofile');
        }

        $poem = new TblPoems();
        $composePoem = $this->createForm(ComposePoem::class, $poem);

        $composePoem->handleRequest($req);
        if ($composePoem->isSubmitted() && $composePoem->isValid()){

            $data = $composePoem->getData();

            $em = $this->getDoctrine()->getManager();

            $poems->setTitle($data->getTitle());
            $poems->setPrivate($data->getPrivate());
            $poems->setPoem($data->getPoem());
            $poems->setActId($this->session->get("act_id"));
            $poems->setPoemDate(new \DateTime());
            $em->persist($poems);

            $em->flush();

            return $this->redirectToRoute('myprofile');
        }

        $info = $this->getDoctrine()
        ->getRepository(TblAccounts::class)
        ->findByActId($act_id);

        
        $mypoems = $this->getDoctrine()->getManager()->createQuery(
            'SELECT poems.title, poems.poem, poems.poemId
            FROM AppBundle:TblPoems poems, AppBundle:TblAccounts accts
            WHERE poems.actId = accts.actId and poems.actId = '. $this->session->get('act_id')
        )->getResult();

         

        return $this->render('default/myprofile.html.twig', ['composePoem'=>$composePoem->createView(), 'myprofile'=>$myprofile->createView(), 'infos'=>$info, 'mypoems'=>$mypoems]);
    }

    /**
     * @Route("/home", name="home")
     */
    public function homeFunction(Request $req)
    {

        if (!$this->session->has("act_id")) {
            return new RedirectResponse('/');
        }

        $poems = new TblPoems();
        $composePoem = $this->createForm(ComposePoem::class, $poems);

        $composePoem->handleRequest($req);
        if ($composePoem->isSubmitted() && $composePoem->isValid()){
            
            

            $data = $composePoem->getData();

            $em = $this->getDoctrine()->getManager();

            $poems->setTitle($data->getTitle());
            $poems->setPrivate($data->getPrivate());
            $poems->setPoem($data->getPoem());
            $poems->setActId($this->session->get("act_id"));
            $poems->setPoemDate(new \DateTime());
            $em->persist($poems);

            $em->flush();

            return $this->redirectToRoute('home');
        }

        // $allPoems = $this->getDoctrine()
        // ->getRepository(TblPoems::class)
        // ->findAll();

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            'SELECT poems.title, poems.poem, accts.fname
            FROM AppBundle:TblPoems poems, AppBundle:TblAccounts accts
            WHERE poems.actId = accts.actId and poems.private != 1 order by poems.poemDate desc'
        );

        $allPoems = $query->getResult();

        return $this->render('default/home.html.twig', ['composePoem'=>$composePoem->createView(), 'allPoems'=>$allPoems, 'name'=>$this->session->get('name')]);
    }

    /**
     * @Route("/deletePoem/{id}")
     */
    public function deletePoem($id)
    {
        $em = $this->getDoctrine()->getManager();
	    $poem = $em->getRepository(TblPoems::class) -> find($id);
	    if (!$poem){
		    throw $this->createNotFoundException("Not Found!");
	    }
        $em->remove($poem);
        $em->flush();

        
        return new RedirectResponse('/myprofile');
    }

    /**
     * @Route("/logout")
     */
    public function logout()
    {
        $this->session->clear();
        return new RedirectResponse('/');
    }

    
}
