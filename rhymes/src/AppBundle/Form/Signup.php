<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
class Signup extends AbstractType
{
    public function buildForm(FormBuilderInterface $fbi, array $opt) {
        $fbi
        ->add('fname', TextType::class, array('label'=> false))
        ->add('lname', TextType::class, array('label'=> false))
        ->add('actEmail', EmailType::class, array('label'=> false))
        ->add('actPassword', PasswordType::class, array('label'=> false))
        ->add('bday', BirthdayType::class, array('label'=> false))
        ->add('signup', SubmitType::class);
    } 
}
?>