<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

use Symfony\Component\Form\Extension\Core\Type\PasswordType;
class Login extends AbstractType
{
    public function buildForm(FormBuilderInterface $fbi, array $opt) {
        $fbi
        ->add('actEmail', EmailType::class, array('label'=> false))
        ->add('actPassword', PasswordType::class, array('label'=> false))
        ->add('Login', SubmitType::class);
    } 
}
?>