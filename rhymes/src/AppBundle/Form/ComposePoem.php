<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
class ComposePoem extends AbstractType
{
    public function buildForm(FormBuilderInterface $fbi, array $opt) {
        $fbi->add('title', TextType::class)
        ->add('private', CheckboxType::class, array('required'=>false))
        ->add('poem', TextareaType::class)
        ->add('Post', SubmitType::class);
    }
}
?>