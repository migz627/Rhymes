<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TblAccounts
 *
 * @ORM\Table(name="tbl_accounts")
 * @ORM\Entity
 */
class TblAccounts
{
    /**
     * @var string
     *
     * @ORM\Column(name="act_email", type="string", length=255, nullable=true)
     */
    private $actEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="act_password", type="string", length=255, nullable=true)
     */
    private $actPassword;

    /**
     * @var string
     *
     * @ORM\Column(name="fname", type="string", length=255, nullable=true)
     */
    private $fname;

    /**
     * @var string
     *
     * @ORM\Column(name="lname", type="string", length=255, nullable=true)
     */
    private $lname;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="bday", type="date", nullable=true)
     */
    private $bday;

    /**
     * @var string
     *
     * @ORM\Column(name="bio", type="text", nullable=true)
     */
    private $bio;

    /**
     * @var integer
     *
     * @ORM\Column(name="act_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $actId;
    

    function setActId($id) {
        $this->actId = $id;
    }

    function getActId() {
        return $this->actId;
    }

    function setActEmail($email) {
        $this->actEmail = $email;
    }

    function getActEmail() {
        return $this->actEmail;
    }

    function setActPassword($password) {
        $this->actPassword = $password;
    }

    function getActPassword() {
        return $this->actPassword;
    }

    function setFname($fname) {
        $this->fname = $fname;
    }

    function getFname() {
        return $this->fname;
    }

    function setLname($lname) {
        $this->lname = $lname;
    }

    function getLname() {
        return $this->lname;
    }

    function setBday($bday) {
        $this->bday = $bday;
    }

    function getBday() {
        return $this->bday;
    }

    function setBio($bio) {
        $this->bio = $bio;
    }

    function getBio() {
        return $this->bio;
    }
}

