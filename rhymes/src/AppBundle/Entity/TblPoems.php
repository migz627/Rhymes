<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TblPoems
 *
 * @ORM\Table(name="tbl_poems")
 * @ORM\Entity
 */
class TblPoems
{
    /**
     * @var string
     *
     * @ORM\Column(name="poem", type="text", nullable=true)
     */
    private $poem;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var integer
     *
     * @ORM\Column(name="private", type="integer", nullable=true)
     */
    private $private;

    /**
     * @var integer
     *
     * @ORM\Column(name="act_id", type="integer", nullable=true)
     */
    private $actId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="poem_date", type="datetime", nullable=true)
     */
    private $poemDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="poem_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $poemId;

    function setPrivate($id) {
        $this->private = $id;
    }

    function getPrivate() {
        return $this->private;
    }

    function setTitle ($title) {
        $this->title = $title;
    }

    function getTitle () {
        return $this->title;
    }

    function setPoem ($poem) {
        $this->poem = $poem;
    }

    function getPoem () {
        return $this->poem;
    }

    function setActId ($id) {
        $this->actId = $id;
    }

    function getActId () {
        return $this->actId;
    }

    function setPoemDate ($date) {
        $this->poemDate = $date;
    }

    function getPoemDate () {
        return $this->poemDate;
    }

}

